require('../models/Tasks');
const requireLogin = require('../middleware/requireLogin');
const dbOps = require('../db');
module.exports = app => {
  app.get('/api/list', requireLogin, async (req, res) => {
    const tasks = await dbOps.listTasks(req.user._id);
    res.send(tasks);
  });

  app.post('/api/new-task', requireLogin, async (req, res) => {
    const { title, tags } = req.body;
    const addedTask = await dbOps.addTask(title, tags, req.user._id);
    res.send(addedTask._id);
  });

  app.patch('/api/rename-task', requireLogin, async (req, res) => {
    const { id, title, tags } = req.body;
    await dbOps.renameTask(id, title, tags);
    res.sendStatus(200);
  });

  app.patch('/api/toggle-task', requireLogin, async (req, res) => {
    const { id } = req.body;
    await dbOps.toggleTask(id);
    res.sendStatus(200);
  });

  app.patch('/api/add-pomo', requireLogin, async (req, res) => {
    const { id, startedAt, finishedAt } = req.body;
    await dbOps.addPomo(id, startedAt, finishedAt);
    res.sendStatus(200);
  });

  app.patch('/api/add-interrupted', requireLogin, async (req, res) => {
    const { id, interruptedPomos } = req.body;
    await dbOps.addInterrupted(id, interruptedPomos);
    res.sendStatus(200);
  });
};
