const mongoose = require("mongoose");
const { Schema } = mongoose;

const usersSchema = new Schema({
  lastname: String,
  firstname: String,
  googleId: String,
  photo: String,
  emails: [String]
});

mongoose.model("users", usersSchema);
