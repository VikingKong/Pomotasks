const mongoose = require("mongoose");
const { Schema } = mongoose;

const pomoSchema = new Schema(
  {
    startedAt: Number,
    finishedAt: Number
  },
  { _id: false }
);

const tasksSchema = new Schema({
  _user: { type: Schema.Types.ObjectId, ref: "User" },
  title: String,
  finished: Boolean,
  pomodoros: [pomoSchema],
  tags: [String]
});

mongoose.model("tasks", tasksSchema);
