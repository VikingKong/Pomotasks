import React, { Component } from 'react';
import { overallTime, pomoTime } from './helpers';

class ActiveDateItem extends Component {
  render() {
    return (
      <div>
        <li className="list-group-item list-group-item-date">
          <div className="list-item">
            <div className="date-item-text">
              <h6>{this.props.date}</h6>
            </div>
            <span className="badge"> Pomos finished: {this.props.pomos.length}</span>
            <span className="badge">
              {' '}
              Time total:{' '}
              {overallTime(
                this.props.pomos
                  .map(pomo => Math.round((pomo.finishedAt - pomo.startedAt) / 1000))
                  .reduce((a, b) => a + b, 0)
              )}
            </span>
          </div>
        </li>
        {this.props.pomos.map((pomo, index) => {
          return (
            <li key={index} className="list-group-item list-group-item-pomo">
              <div className="list-item">
                <div className="finished-pomodoros-date-field">{pomoTime(pomo.startedAt, pomo.finishedAt)}</div>
                <div className="finished-pomodoros-title-field">
                  {pomo.title}
                  <p />
                  {Array.from(pomo.tags || new Set()).map((item, key) => {
                    return (
                      <span className="tag" key={key}>
                        {' #' + item}
                      </span>
                    );
                  })}
                </div>
              </div>
            </li>
          );
        })}
      </div>
    );
  }
}

export default ActiveDateItem;
