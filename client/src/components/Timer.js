import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { timeToBeDisplayed } from './helpers';
import pomoImage from '../assets/tomato.png';
import playButton from '../assets/play-button.svg';
import pauseButton from '../assets/pause.svg';
import stopButton from '../assets/stop.svg';
import cancelButton from '../assets/cancel.svg';

class Timer extends Component {
  render() {
    let result = timeToBeDisplayed(this.props.pomoState.secondsLeft);
    let stopPauseButtonLabel = this.props.pomoState.state === 'PAUSED' ? playButton : pauseButton;
    let stopPauseButtonStyle =
      this.props.pomoState.state === 'PAUSED' ? 'btn btn-primary timer-button' : 'btn btn-warning timer-button';
    let stopPauseButtonAction =
      this.props.pomoState.state === 'PAUSED' ? this.props.continueTimer : this.props.pauseTimer;
    return (
      <div className="timer">
        <img src={pomoImage} alt="" />
        <div className="timer-text">
          <div className="timer-time">
            <h2>{result}</h2>
          </div>
        </div>
        {this.props.pomoState.state === 'RUNNING' || this.props.pomoState.state === 'PAUSED' ? (
          <div className="timer-buttons">
            <Button className={stopPauseButtonStyle} onClick={() => stopPauseButtonAction()}>
              <img src={stopPauseButtonLabel} alt="" />
            </Button>{' '}
            <Button className="btn btn-success timer-button" onClick={() => this.props.stopTimer()}>
              <img src={stopButton} alt="" />
            </Button>{' '}
            <Button className="btn btn-danger timer-button" onClick={() => this.props.cancelTimer()}>
              <img src={cancelButton} alt="" />
            </Button>
          </div>
        ) : (
          <div />
        )}
      </div>
    );
  }
}

export default Timer;
