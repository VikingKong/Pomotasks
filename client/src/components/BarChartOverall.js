import { connect } from 'react-redux';
import BarChartFactory from './BarChartFactory';

class BarChartOverall extends BarChartFactory {
  initXAxis() {
    const today = new Date(Date.now());
    return [today.getFullYear().toString()];
  }

  wrangleData() {
    let dataSetObj = {};

    if (!this.props.finishedPomos.length) {
      this.dataSet = [];
      return;
    }

    this.props.finishedPomos.forEach(p => {
      const currentPomoYear = new Date(p.finishedAt).getFullYear();
      const currentPomoTimeSpent = p.finishedAt - p.startedAt;
      if (!dataSetObj.hasOwnProperty(currentPomoYear))
        dataSetObj[currentPomoYear] = currentPomoTimeSpent / 1000 / 60 / 60;
      else dataSetObj[currentPomoYear] += currentPomoTimeSpent / 1000 / 60 / 60;
    });

    this.dataSet = Object.keys(dataSetObj)
      .map(e => {
        return { date: e, timeSpent: dataSetObj[e] };
      })
      .sort((a, b) => a.date - b.date)
      .map(e => {
        return { date: e.date.toString(), timeSpent: e.timeSpent };
      });
  }
}

function mapStateToProps(state) {
  return { finishedPomos: state.finishedPomos };
}

export default connect(
  mapStateToProps,
  null
)(BarChartOverall);
