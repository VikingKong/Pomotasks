import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import * as d3Tip from 'd3-tip';
import * as d3 from 'd3';
import { overallTime } from './helpers';

class DonutChartFactory extends Component {
  constructor() {
    super();
    this.title = '';
  }

  wrangleData() {
    this.dataSet = [{ name: 'Nothing here', timeSpent: 1, emptyData: true }];
  }

  prepareViz(el) {
    this.wrangleData();
    this.margin = { top: 50, right: 0, bottom: 60, left: 70 };
    this.width = 950 - this.margin.left - this.margin.right;
    this.height = 950 - this.margin.top - this.margin.bottom;
    this.radius = Math.min(this.width, this.height) / 2;
    this.color = d3.scaleOrdinal(d3.schemeCategory10);

    this.arc = d3
      .arc()
      .innerRadius(this.radius - 90)
      .outerRadius(this.radius - 30);

    this.pie = d3
      .pie()
      .padAngle(0.03)
      .value(function(d) {
        return d.timeSpent;
      })
      .sort(null);

    this.svg = d3
      .select(this.el)
      .append('svg')
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr(
        'viewBox',
        '0 0 ' +
          (this.width + this.margin.left + this.margin.right) +
          ' ' +
          (this.height + this.margin.top + this.margin.bottom)
      )
      .style('width', 600)
      .style('height', 600);

    this.g = this.svg
      .selectAll('.arc')
      .data(this.pie(this.dataSet))
      .enter()
      .append('g')
      .attr(
        'transform',
        'translate(' +
          (this.margin.left + this.width / 2) +
          ', ' +
          (this.margin.top + this.height / 2) +
          ')'
      )
      .attr('class', 'arc');

    this.tip = d3Tip
      .default()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(
        d =>
          `<span style="color:white"><strong>${d.data.name}${
            !d.data.emptyData ? ': ' + overallTime(d.data.timeSpent / 1000) : ''
          }</strong></span>`
      );

    this.g
      .append('path')
      .attr('d', this.arc)
      .style('fill', d => this.color(d.data.name))
      .on('mouseover', this.tip.show)
      .on('mouseout', this.tip.hide);

    this.g.call(this.tip);
  }

  updateViz() {
    const vis = this;
    vis.wrangleData();
    vis.path = vis.g.selectAll('path');

    vis.data0 = vis.path.data();
    vis.data1 = vis.pie(vis.dataSet);

    vis.path = vis.path.data(vis.data1, key);

    vis.path
      .exit()
      .datum(function(d, i) {
        return findNeighborArc(i, vis.data1, vis.data0, key) || d;
      })
      .transition()
      .duration(50)
      .attrTween('d', arcTween)
      .remove();

    vis.path
      .transition()
      .duration(50)
      .attrTween('d', arcTween);

    vis.path
      .enter()
      .append('path')
      .each(function(d, i) {
        vis._current = findNeighborArc(i, vis.data0, vis.data1, key) || d;
      })
      .attr('fill', function(d) {
        return vis.color(key(d));
      })
      .on('mouseover', vis.tip.show)
      .on('mouseout', vis.tip.hide)
      .transition()
      .duration(50)
      .attrTween('d', arcTween);

    function key(d) {
      return d.data.name;
    }

    function findNeighborArc(i, data0, data1, key) {
      let d;
      d = findPreceding(i, vis.data0, vis.data1, key);
      if (d) return { startAngle: d.endAngle, endAngle: d.endAngle };
      d = findFollowing(i, vis.data0, vis.data1, key);
      if (d) return { startAngle: d.startAngle, endAngle: d.startAngle };
      return null;
    }

    function findPreceding(i, data0, data1, key) {
      let m = vis.data0.length;
      while (--i >= 0) {
        if (!vis.data1[i]) i = 0;
        let k = key(vis.data1[i]);
        for (let j = 0; j < m; ++j) {
          if (key(vis.data0[j]) === k) return vis.data0[j];
        }
      }
    }

    function findFollowing(i, data0, data1, key) {
      let n = vis.data1.length,
        m = vis.data0.length;
      while (++i < n) {
        let k = key(vis.data1[i]);
        for (let j = 0; j < m; ++j) {
          if (key(vis.data0[j]) === k) return vis.data0[j];
        }
      }
    }

    function arcTween(d) {
      let i = d3.interpolate(vis._current, d);
      vis._current = i(1);
      return function(t) {
        return vis.arc(i(t));
      };
    }
  }

  componentDidMount() {
    this.el = ReactDOM.findDOMNode(this);
    const position = this.el.getBoundingClientRect();
    this.x = position.x;
    this.y = position.y;
    this.prepareViz(this.el);
  }

  componentDidUpdate() {
    this.updateViz();
  }

  render() {
    return (
      <div>
        <h5>{this.title}</h5>
      </div>
    );
  }
}

export default DonutChartFactory;
