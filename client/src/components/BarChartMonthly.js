import BarChartFactory from './BarChartFactory';

class BarChartMonthly extends BarChartFactory {
  initXAxis() {
    let xDomain = [];
    for (let i = 1; i <= this.props.numOfDays; i++) {
      xDomain.push(i.toString());
    }
    return xDomain;
  }

  wrangleData() {
    let dataSetObj = {};
    for (let i = 1; i <= this.props.numOfDays; i++) {
      dataSetObj[i] = 0;
    }

    this.props.filteredPomos.forEach(p => {
      const currentPomoDate = new Date(p.finishedAt).getDate();
      const currentPomoTimeSpent = p.finishedAt - p.startedAt;
      dataSetObj[currentPomoDate] += currentPomoTimeSpent / 1000 / 60 / 60;
    });

    this.currentTick = new Date(Date.now()).getDate().toString();
    this.dataSet = Object.keys(dataSetObj)
      .map(e => {
        return { date: e, timeSpent: dataSetObj[e] };
      })
      .sort((a, b) => parseInt(a, 10) - parseInt(b, 10));
  }
}

export default BarChartMonthly;
