import pomoImage from '../assets/tomato.png';

export function lastPomo(task) {
  const pomoArr = task.pomodoros;
  const numPomos = pomoArr.length;
  if (numPomos === 0) return 0;
  return pomoArr[numPomos - 1].finishedAt;
}

export function setLeadingZero(s) {
  let result = s;
  if (s < 10) {
    result = '0' + s;
  }
  return result;
}

export function timeToBeDisplayed(secs) {
  let seconds = secs;
  let minutes = Math.floor(seconds / 60);
  seconds -= minutes * 60;
  return `${setLeadingZero(minutes)}:${setLeadingZero(seconds)}`;
}

function createTimeStructure(t) {
  let tObj = new Date(t);
  let year = tObj.getFullYear();
  let month = months[tObj.getMonth()];
  let date = setLeadingZero(tObj.getDate());
  let hour = setLeadingZero(tObj.getHours());
  let minutes = setLeadingZero(tObj.getMinutes());
  let seconds = setLeadingZero(tObj.getSeconds());
  return { year, month, date, hour, minutes, seconds };
}

export function pomoTime(s, f) {
  let pomoStructStarted = createTimeStructure(s);
  let pomoStructFinished = createTimeStructure(f);
  return `${pomoStructStarted.hour}:${pomoStructStarted.minutes}:${pomoStructStarted.seconds} - ${
    pomoStructFinished.hour
  }:${pomoStructFinished.minutes}:${pomoStructFinished.seconds}\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0`;
}

export function overallTime(s) {
  let hours = Math.floor(s / 3600);
  let minutes = Math.floor((s - hours * 3600) / 60);
  let seconds = Math.floor(s - hours * 3600 - minutes * 60);
  return `${hours}h ${setLeadingZero(minutes)}m ${setLeadingZero(seconds)}s`;
}

export function makeDateString(t) {
  let timeStruct = createTimeStructure(t);
  return `${timeStruct.month}, ${timeStruct.date}\u00A0\u00A0\u00A0${timeStruct.year}`;
}

export function activeDates(pomos) {
  let datesSet = new Set(
    pomos.sort((a, b) => b.finishedAt - a.finishedAt).map(pomo => makeDateString(pomo.finishedAt))
  );
  return [...datesSet];
}

export const months = {
  0: 'January',
  1: 'February',
  2: 'March',
  3: 'April',
  4: 'May',
  5: 'June',
  6: 'July',
  7: 'August',
  8: 'September',
  9: 'October',
  10: 'November',
  11: 'December'
};

export function vizCurrentMonth(month, year) {
  const currentDate = new Date(year, month);
  return `${months[currentDate.getMonth()]}\u00A0\u00A0\u00A0${currentDate.getFullYear()}`;
}

function notificationOptions(taskName) {
  return {
    body: 'Another pomodoro in ' + taskName + ' has been finished!',
    icon: pomoImage
  };
}

export function showNotification(taskName) {
  let notify;
  if (Notification.permission === 'granted') {
    notify = new Notification('Pomotasks', notificationOptions(taskName));
    notify.onclick = function() {
      window.focus();
      notify.close();
    };
  } else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function(permission) {
      if (permission === 'granted') {
        notify = new Notification('Pomotasks', notificationOptions(taskName));
        notify.onclick = function() {
          window.focus();
          notify.close();
        };
      }
    });
  }
}

export function getNumberOfDaysInMonth(offset) {
  const today = new Date(Date.now());
  const year = today.getFullYear();
  const month = today.getMonth();
  return new Date(year, month + 1 + offset, 0).getDate();
}
