import { connect } from "react-redux";
import { createNewTask } from "../actions";
import TaskInputFactory from "./TaskInputFactory";

class NewTask extends TaskInputFactory {
  onSubmit() {
    this.props.createNewTask(this.state.taskName, this.state.tagsAssigned);
    this.setState({ taskName: "", tagsAssigned: new Set() });
    this.hideTagEditor();
  }
}

export default connect(
  null,
  { createNewTask }
)(NewTask);
