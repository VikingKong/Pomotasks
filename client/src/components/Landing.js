import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { fetchUser } from "../actions";
import pomoImage from "../assets/tomato.png";

class Landing extends Component {
  componentDidMount() {
    this.props.fetchUser(this.props.history);
  }

  render() {
    return (
      <div className="App landing">
        <h1>Pomotasks</h1>
        <img src={pomoImage} alt="" />
        <h2>Track your time with the Pomodoro Technique</h2>
      </div>
    );
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default withRouter(
  connect(
    mapStateToProps,
    { fetchUser }
  )(Landing)
);
