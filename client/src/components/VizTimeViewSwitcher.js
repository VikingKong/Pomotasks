import React, { Component } from 'react';
import { ToggleButton, ToggleButtonGroup } from 'react-bootstrap';

class VizTimeViewSwither extends Component {
  handleChange(val, event) {
    this.props.toggleView(val);
  }

  render() {
    return (
      <div>
        <ToggleButtonGroup
          type="radio"
          defaultValue={this.props.timeViewType}
          name="timeviews"
          onChange={this.handleChange.bind(this)}
        >
          <ToggleButton value={1}>Daily</ToggleButton>
          <ToggleButton value={2}>Weekly</ToggleButton>
          <ToggleButton value={3}>Monthly</ToggleButton>
          <ToggleButton value={4}>Yearly</ToggleButton>
          <ToggleButton value={5}>Overall</ToggleButton>
        </ToggleButtonGroup>
      </div>
    );
  }
}

export default VizTimeViewSwither;
