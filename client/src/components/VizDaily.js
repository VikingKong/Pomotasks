import React, { Component } from 'react';
import { connect } from 'react-redux';
import { dayForward, dayBackward } from '../actions';
import BarChartDaily from './BarChartDaily';
import VizTimeFrameSwitcher from './VizTimeFrameSwitcher';
import DonutChartTasks from './DonutChartTasks';
import DonutChartTags from './DonutChartTags';
import { months } from './helpers';

class VizDaily extends Component {
  constructor() {
    super();
    const today = new Date(Date.now());
    const date = today.getDate();
    const year = today.getFullYear();
    const month = today.getMonth();
    this.state = {
      timeFrameLabel: `${months[month]},\u00A0${date}\u00A0\u00A0${year}`,
      filteredPomos: []
    };
  }
  componentDidMount() {
    let currentFrame = this.cutTimeFrame(this.props.finishedPomos);
    this.setState(currentFrame);
  }

  componentWillReceiveProps(newProps) {
    let currentFrame = this.cutTimeFrame(newProps.finishedPomos);
    this.setState(currentFrame);
  }

  cutTimeFrame(data) {
    const today = new Date(Date.now());
    const date = today.getDate() + this.props.dailyVizFilter;
    const year = today.getFullYear();
    const month = today.getMonth();
    const leftBoundary = new Date(year, month, date, 0, 0, 0, 0).getTime();
    const rightBoundary = new Date(year, month, date, 23, 59, 59, 999).getTime();
    let filteredPomos = data.filter(p => p.finishedAt >= leftBoundary && p.finishedAt <= rightBoundary);
    const currentDate = new Date(year, month, date);
    const timeFrameLabel = `${
      months[currentDate.getMonth()]
    },\u00A0${currentDate.getDate()}\u00A0\u00A0${currentDate.getFullYear()}`;
    return { filteredPomos, timeFrameLabel };
  }

  async forwardAction() {
    await this.props.dayForward();
    let currentFrame = this.cutTimeFrame(this.props.finishedPomos);
    this.setState(currentFrame);
  }

  async backwardAction() {
    await this.props.dayBackward();
    let currentFrame = this.cutTimeFrame(this.props.finishedPomos);
    this.setState(currentFrame);
  }

  render() {
    return (
      <div className="viz">
        <div className="row">
          <div className="col-md-12">
            <VizTimeFrameSwitcher
              timeFrameLabel={this.state.timeFrameLabel}
              forwardAction={this.forwardAction.bind(this)}
              backwardAction={this.backwardAction.bind(this)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-4">
            <BarChartDaily filteredPomos={this.state.filteredPomos} />
          </div>
          <div className="col-md-4">
            <DonutChartTasks filteredPomos={this.state.filteredPomos} />
          </div>
          <div className="col-md-4">
            <DonutChartTags filteredPomos={this.state.filteredPomos} />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { dailyVizFilter: state.dailyVizFilter, finishedPomos: state.finishedPomos };
}

export default connect(
  mapStateToProps,
  { dayForward, dayBackward }
)(VizDaily);
