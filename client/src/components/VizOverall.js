import React, { Component } from 'react';
import { connect } from 'react-redux';
import BarChartOverall from './BarChartOverall';
import DonutChartTasks from './DonutChartTasks';
import DonutChartTags from './DonutChartTags';

class VizOverall extends Component {
  constructor() {
    super();
    this.state = { finishedPomos: [] };
  }

  componentDidMount() {
    this.setState({ finishedPomos: this.props.finishedPomos });
  }

  componentWillReceiveProps(newProps) {
    this.setState({ finishedPomos: newProps.finishedPomos });
  }

  render() {
    return (
      <div className="viz">
        <div className="before-viz" />
        <div className="row">
          <div className="col-md-4">
            <BarChartOverall filteredPomos={this.state.finishedPomos} />
          </div>
          <div className="col-md-4">
            <DonutChartTasks filteredPomos={this.state.finishedPomos} />
          </div>
          <div className="col-md-4">
            <DonutChartTags filteredPomos={this.state.finishedPomos} />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { finishedPomos: state.finishedPomos };
}

export default connect(
  mapStateToProps,
  null
)(VizOverall);
