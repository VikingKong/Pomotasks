import React, { Component } from "react";
import { connect } from "react-redux";
import { SplitButton, MenuItem } from "react-bootstrap";
import { filterFinishedPomodoros } from "../actions";

class FinishedPomosFilter extends Component {
  constructor() {
    super();
    this.state = { title: "Last 7 days" };
    this.timeFrames = {
      1: 86400 * 1000 * 7,
      2: 86400 * 1000 * 14,
      3: 86400 * 1000 * 30,
      4: 86400 * 1000 * 60,
      5: 86400 * 1000 * 90,
      6: 86400 * 1000 * 180,
      7: 86400 * 1000 * 365,
      8: null
    };
  }

  clickHandler(eventKey, event) {
    this.setState({ title: event.target.innerHTML });
    this.props.filterFinishedPomodoros(this.timeFrames[eventKey]);
  }

  render() {
    return (
      <div className="filter-finished-pomos-dropdown">
        <SplitButton
          bsStyle="default"
          title={this.state.title}
          id="filterFinished"
        >
          <MenuItem onSelect={this.clickHandler.bind(this)} eventKey="1">
            Last 7 days
          </MenuItem>
          <MenuItem onSelect={this.clickHandler.bind(this)} eventKey="2">
            Last 14 days
          </MenuItem>
          <MenuItem onSelect={this.clickHandler.bind(this)} eventKey="3">
            Last 30 days
          </MenuItem>
          <MenuItem onSelect={this.clickHandler.bind(this)} eventKey="4">
            Last 60 days
          </MenuItem>
          <MenuItem onSelect={this.clickHandler.bind(this)} eventKey="5">
            Last 90 days
          </MenuItem>
          <MenuItem onSelect={this.clickHandler.bind(this)} eventKey="6">
            Last 180 days
          </MenuItem>
          <MenuItem onSelect={this.clickHandler.bind(this)} eventKey="7">
            Last 365 days
          </MenuItem>
          <MenuItem onSelect={this.clickHandler.bind(this)} eventKey="8">
            All time
          </MenuItem>
        </SplitButton>
      </div>
    );
  }
}

export default connect(
  null,
  { filterFinishedPomodoros }
)(FinishedPomosFilter);
