import React, { Component } from 'react';
import { Checkbox } from 'react-bootstrap';
import { connect } from 'react-redux';
import TaskItem from './TaskItem.js';

class TaskList extends Component {
  constructor() {
    super();
    this.state = {
      showFinishedTasks: false
    };
  }

  render() {
    return (
      <div>
        <ul className="list-group">
          {this.props.tasks
            .filter(task => (!this.state.showFinishedTasks ? !task.finished : true))
            .map((task, index, arr) => {
              return (
                <TaskItem
                  key={task._id}
                  task={task}
                  startTimer={this.props.startTimer}
                  pomoState={this.props.pomoState}
                  currentTask={this.props.currentTask}
                  isLast={index === arr.length - 1 && arr.length > 1}
                />
              );
            })}
        </ul>
        {this.props.tasks.length > 0 ? (
          <Checkbox
            className="finished-tasks-checkbox"
            checked={this.state.showFinishedTasks}
            onChange={() => {
              this.state.showFinishedTasks
                ? this.setState({ showFinishedTasks: false })
                : this.setState({ showFinishedTasks: true });
            }}
          >
            Show finished tasks
          </Checkbox>
        ) : (
          <div />
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    tasks: state.tasks
  };
}

export default connect(
  mapStateToProps,
  null
)(TaskList);
