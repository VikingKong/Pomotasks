import React, { Component } from "react";
import { connect } from "react-redux";
import ActiveDateItem from "./ActiveDateItem";
import { activeDates, makeDateString } from "./helpers";

class PomoList extends Component {
  render() {
    return (
      <div>
        <div className="pomos-list-title">
          <h4>Finished Pomodoros</h4>
        </div>
        <ul className="list-group">
          {activeDates(
            this.props.pomos.filter(a =>
              this.props.lastNDays
                ? Date.now() - a.finishedAt < this.props.lastNDays
                : true
            )
          ).map((dt, index) => {
            return (
              <ActiveDateItem
                key={index}
                date={dt}
                pomos={this.props.pomos.filter(
                  pomo => makeDateString(pomo.finishedAt) === dt
                )}
                className="list-group-item"
              />
            );
          })}
        </ul>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    pomos: state.finishedPomos,
    lastNDays: state.lastNDays
  };
}

export default connect(
  mapStateToProps,
  null
)(PomoList);
