import BarChartFactory from './BarChartFactory';

class BarChartDaily extends BarChartFactory {
  initXAxis() {
    return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
  }
  
  wrangleData() {
    let dataSetObj = {};
    for (let i = 0; i <= 23; i++) {
      dataSetObj[i] = 0;
    }

    this.props.filteredPomos.forEach(p => {
      const currentPomoHour = new Date(p.finishedAt).getHours();
      const currentPomoTimeSpent = p.finishedAt - p.startedAt;
      dataSetObj[currentPomoHour] += currentPomoTimeSpent / 1000 / 60 / 60;
    });
    this.currentTick = new Date(Date.now()).getHours().toString();
    this.dataSet = Object.keys(dataSetObj)
      .map(e => {
        return { date: e.toString(), timeSpent: dataSetObj[e] };
      })
      .sort((a, b) => a.date - b.date);
  }
}

export default BarChartDaily;
