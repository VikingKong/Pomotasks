import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import TaskList from './TaskList';
import Timer from './Timer';
import PomoList from './PomoList';
import NewTask from './NewTask';
import FinishedPomosFilter from './FinishedPomosFilter';
import { fetchUser } from '../actions';
import StartAutomatically from './StartAutomatically';
import './App.css';
import './queries.css';

class Dashboard extends Component {
  componentDidMount() {
    this.props.fetchUser(this.props.history, '/dashboard');
  }

  render() {
    return (
      <div className="App">
        <div className="row">
          <div className="col-md-12">
            <Timer
              pomoState={this.props.pomoState}
              pauseTimer={this.props.pauseTimer}
              continueTimer={this.props.continueTimer}
              cancelTimer={this.props.cancelTimer}
              stopTimer={this.props.stopTimer}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <StartAutomatically setStartAutomatically={this.props.setStartAutomatically} />
          </div>
          <div className="col-md-6">
            <FinishedPomosFilter />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <div className="text-input-container">
              <NewTask />
            </div>
            <TaskList
              startTimer={this.props.startTimer}
              pomoState={this.props.pomoState.state}
              currentTask={this.props.taskId}
            />
          </div>
          <div className="col-md-6">
            <PomoList />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default withRouter(
  connect(
    mapStateToProps,
    { fetchUser }
  )(Dashboard)
);
