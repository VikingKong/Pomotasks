import React, { Component } from "react";
import { Overlay } from "react-bootstrap";
import { FormControl, InputGroup, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { addTag } from "../actions";
import TagEntry from "./TagEntry.js";

class PopOver extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newTags: ""
    };
  }

  render() {
    return (
      <div
        className={this.props.className}
        style={{
          ...this.props.style,
          position: "absolute",
          backgroundColor: "black",
          border: "1px solid gray",
          borderRadius: 3,
          minHeight: "50px"
        }}
      >
        <div className="tag-input">
          <InputGroup>
            <FormControl
              type="text"
              placeholder="New Tag(s), use comma as a delimiter"
              value={this.state.newTags}
              onChange={event => {
                this.setState({ newTags: event.target.value });
              }}
            />
            <InputGroup.Button>
              <Button
                onClick={() => {
                  if (this.state.newTags) {
                    for (let tag of this.state.newTags
                      .split(",")
                      .filter(e => e)) {
                      this.props.addTag(tag);
                    }
                    this.setState({ newTags: "" });
                  }
                }}
              >
                Add Tag(s)
              </Button>
            </InputGroup.Button>
          </InputGroup>
        </div>
        <div className="tags-field">
          {this.props.tags.map((item, key) => {
            return (
              <TagEntry
                key={key}
                name={item}
                tagsAssigned={this.props.tagsAssigned}
                assignTag={this.props.assignTag}
                dismissTag={this.props.dismissTag}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

class TagEditor extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      show: false
    };
  }

  componentWillReceiveProps(props) {
    this.setState({
      show: props.show
    });
  }

  render() {
    return (
      <Overlay
        show={this.state.show}
        placement={!this.props.isLast ? "bottom" : "top"}
        container={this}
        target={this.props.target}
      >
        <PopOver
          tags={Array.from(this.props.tags).sort()}
          addTag={this.props.addTag}
          assignTag={this.props.assignTag}
          dismissTag={this.props.dismissTag}
          tagsAssigned={this.props.tagsAssigned || new Set()}
        />
      </Overlay>
    );
  }
}

function mapStateToProps(state) {
  return {
    tags: state.tags
  };
}

export default connect(
  mapStateToProps,
  { addTag }
)(TagEditor);
