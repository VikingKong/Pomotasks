import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { overallTime } from './helpers';
import { renameTask, updateTaskStatus } from '../actions';
import UpdateTask from './UpdateTask';
import playButton from '../assets/play-button.svg';

class TaskItem extends Component {
  constructor() {
    super();
    this.state = {
      hovered: false,
      editing: false,
      checked: false,
      taskName: '',
      eh: this.ESCHandler.bind(this)
    };
  }

  componentDidMount() {
    this.setState({
      checked: this.props.task.finished
    });
  }

  ESCHandler(event) {
    if (event.keyCode === 27) {
      this.setState({ editing: false });
      document.removeEventListener('keydown', this.state.eh);
    }
  }

  discardEditor() {
    this.setState({ editing: false });
  }

  buttonStyle() {
    let style = 'btn btn-success';
    return this.state.checked ||
      !this.state.hovered ||
      this.state.editing ||
      this.props.pomoState === 'RUNNING' ||
      this.props.pomoState === 'PAUSED'
      ? (style += ' hidden')
      : style;
  }

  itemStyle() {
    return this.props.currentTask === this.props.task._id &&
      (this.props.pomoState === 'RUNNING' || this.props.pomoState === 'PAUSED')
      ? 'list-group-item list-group-item-started'
      : 'list-group-item list-group-item-left';
  }

  itemText() {
    return this.props.currentTask === this.props.task._id &&
      (this.props.pomoState === 'RUNNING' || this.props.pomoState === 'PAUSED') ? (
      <b style={{ color: 'white' }}>{this.props.task.title}</b>
    ) : (
      this.props.task.title
    );
  }

  render() {
    return (
      <li
        className={this.itemStyle()}
        onMouseOver={() => this.setState({ hovered: true })}
        onMouseOut={() => this.setState({ hovered: false })}
        onDoubleClick={() => {
          document.addEventListener('keydown', this.state.eh);
          this.setState({ editing: true });
        }}
      >
        <div className="list-item">
          {!this.state.editing ? (
            <div>
              <input
                checked={this.state.checked}
                onChange={() => {
                  this.setState({ checked: !this.state.checked });
                  this.props.updateTaskStatus(this.props.task._id, !this.state.checked, this.props.tagsAssined);
                }}
                type="checkbox"
              />
              <div className="list-item-text">
                {this.itemText()}
                <br />
                {Array.from(this.props.task.tags || new Set()).map((item, key) => {
                  return (
                    <span className="tag" key={key}>
                      {' #' + item}
                    </span>
                  );
                })}
              </div>
              <div className="task-badges">
                <span className="badge">Pomos finished: {this.props.task.pomodoros.length}</span>
                <span className="badge">
                  Time total:
                  {overallTime(
                    this.props.task.pomodoros
                      .map(pomo => Math.round((pomo.finishedAt - pomo.startedAt) / 1000))
                      .reduce((a, b) => a + b, 0)
                  )}
                </span>
              </div>
              <div className="right-button">
                <Button
                  className={this.buttonStyle()}
                  onClick={() => {
                    this.props.startTimer(this.props.task._id, this.props.task.title, this.props.task.tags);
                  }}
                >
                  <img src={playButton} alt="" />
                </Button>
              </div>
            </div>
          ) : (
            <UpdateTask
              taskId={this.props.task._id}
              taskName={this.props.task.title}
              tagsAssigned={this.props.task.tags || new Set()}
              isLast={this.props.isLast}
              discardEditor={this.discardEditor.bind(this)}
            />
          )}
        </div>
      </li>
    );
  }
}

export default connect(
  null,
  { renameTask, updateTaskStatus }
)(TaskItem);
