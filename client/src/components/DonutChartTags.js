import DonutChartFactory from './DonutChartFactory';
class DonutChartTags extends DonutChartFactory {
  constructor() {
    super();
    this.title = 'Tags Distribution';
  }

  wrangleData() {
    let dataSetObj = {};

    if (!this.props.filteredPomos.length) {
      this.dataSet = [{ name: 'Nothing here', timeSpent: 1, emptyData: true }];
      return;
    }

    for (let t of new Set(
      this.props.filteredPomos.reduce((a, b) => {
        return [...a, ...Array.from(b.tags)];
      }, [])
    )) {
      dataSetObj[t] = 0;
    }

    this.props.filteredPomos.forEach(p => {
      p.tags.forEach(t => {
        const currentPomoTag = t;
        const currentPomoTimeSpent = p.finishedAt - p.startedAt;
        if (!dataSetObj.hasOwnProperty(currentPomoTag)) dataSetObj[currentPomoTag] = currentPomoTimeSpent;
        else dataSetObj[currentPomoTag] += currentPomoTimeSpent;
      });
    });

    this.dataSet = Object.keys(dataSetObj).map(e => {
      return { name: e, timeSpent: dataSetObj[e], emptyData: false };
    });
  }
}

export default DonutChartTags;
