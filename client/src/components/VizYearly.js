import React, { Component } from 'react';
import { connect } from 'react-redux';
import { yearForward, yearBackward } from '../actions';
import BarChartYearly from './BarChartYearly';
import DonutChartTasks from './DonutChartTasks';
import DonutChartTags from './DonutChartTags';
import VizTimeFrameSwitcher from './VizTimeFrameSwitcher';

class VizYearly extends Component {
  constructor() {
    super();
    const today = new Date(Date.now());
    this.state = {
      timeFrameLabel: today.getFullYear().toString(),
      filteredPomos: []
    };
  }
  componentDidMount() {
    let currentFrame = this.cutTimeFrame(this.props.finishedPomos);
    this.setState(currentFrame);
  }

  componentWillReceiveProps(newProps) {
    let currentFrame = this.cutTimeFrame(newProps.finishedPomos);
    this.setState(currentFrame);
  }

  cutTimeFrame(data) {
    const today = new Date(Date.now());
    const year = today.getFullYear() + this.props.yearlyVizFilter;
    const leftBoundary = new Date(year, 0, 1, 0, 0, 0, 0).getTime();
    const rightBoundary = new Date(year, 11, 31, 23, 59, 59, 999).getTime();
    let filteredPomos = data.filter(p => p.finishedAt >= leftBoundary && p.finishedAt <= rightBoundary);
    const timeFrameLabel = year.toString();
    return { filteredPomos, timeFrameLabel };
  }

  async forwardAction() {
    await this.props.yearForward();
    let currentFrame = this.cutTimeFrame(this.props.finishedPomos);
    this.setState(currentFrame);
  }

  async backwardAction() {
    await this.props.yearBackward();
    let currentFrame = this.cutTimeFrame(this.props.finishedPomos);
    this.setState(currentFrame);
  }

  render() {
    return (
      <div className="viz">
        <div className="row">
          <div className="col-md-12">
            <VizTimeFrameSwitcher
              timeFrameLabel={this.state.timeFrameLabel}
              forwardAction={this.forwardAction.bind(this)}
              backwardAction={this.backwardAction.bind(this)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-4">
            <BarChartYearly filteredPomos={this.state.filteredPomos} />
          </div>
          <div className="col-md-4">
            <DonutChartTasks filteredPomos={this.state.filteredPomos} />
          </div>
          <div className="col-md-4">
            <DonutChartTags filteredPomos={this.state.filteredPomos} />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { yearlyVizFilter: state.yearlyVizFilter, finishedPomos: state.finishedPomos };
}

export default connect(
  mapStateToProps,
  { yearForward, yearBackward }
)(VizYearly);
