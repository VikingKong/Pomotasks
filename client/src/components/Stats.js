import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchUser } from '../actions';
import VizTimeViewSwitcher from './VizTimeViewSwitcher';
import VizMonthly from './VizMonthly';
import VizYearly from './VizYearly';
import VizWeekly from './VizWeekly';
import VizDaily from './VizDaily';
import VizOverall from './VizOverall';
import './App.css';
import './queries.css';
class Stats extends Component {
  constructor() {
    super();
    this.state = { timeViewType: 1 };
  }

  componentDidMount() {
    this.props.fetchUser(this.props.history, '/stats');
  }

  timeViewSelector() {
    switch (this.state.timeViewType) {
      case 1:
        return <VizDaily />;
      case 2:
        return <VizWeekly />;
      case 3:
        return <VizMonthly />;
      case 4:
        return <VizYearly />;
      case 5:
        return <VizOverall />;
      default:
        return <VizDaily />;
    }
  }

  toggleTimeView(val) {
    this.setState({ timeViewType: val });
  }

  render() {
    return (
      <div className="App">
        <div className="row">
          <div className="before-viz" />
        </div>
        <div className="row">
          <VizTimeViewSwitcher toggleView={this.toggleTimeView.bind(this)} timeViewType={this.state.timeViewType} />
        </div>
        <div className="row">{this.timeViewSelector()}</div>
      </div>
    );
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default withRouter(
  connect(
    mapStateToProps,
    { fetchUser }
  )(Stats)
);
