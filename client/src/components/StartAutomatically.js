import React, { Component } from "react";
import { Checkbox } from "react-bootstrap";

class StartAutomatically extends Component {
  constructor() {
    super();
    this.state = {
      checked: false
    };
  }

  componentDidMount() {
    let status = localStorage.startAutomatically;
    if (status) {
      this.setState({ checked: JSON.parse(status) }, () => {
        this.props.setStartAutomatically(this.state.checked);
      });
    }
  }

  render() {
    return (
      <div className="start-automatically-checkbox">
        <Checkbox
          checked={this.state.checked}
          onChange={() => {
            this.setState({ checked: !this.state.checked }, () => {
              this.props.setStartAutomatically(this.state.checked);
              localStorage.startAutomatically = this.state.checked;
            });
          }}
        >
          Start next pomodoro automatically
        </Checkbox>
      </div>
    );
  }
}

export default StartAutomatically;
