import BarChartFactory from './BarChartFactory';

class BarChartWeekly extends BarChartFactory {
  constructor() {
    super();
    this.daysOfWeek = [
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
      'Sunday'
    ];
  }

  initXAxis() {
    return this.daysOfWeek;
  }

  wrangleData() {
    let dataSetObj = {};
    for (let i = 0; i <= 6; i++) {
      dataSetObj[i] = 0;
    }

    this.props.filteredPomos.forEach(p => {
      const currentPomoDay = new Date(p.finishedAt).getDay();
      const currentPomoTimeSpent = p.finishedAt - p.startedAt;
      dataSetObj[currentPomoDay] += currentPomoTimeSpent / 1000 / 60 / 60;
    });
    const currentDay = new Date(Date.now()).getDay();
    this.currentTick = currentDay === 0 ? this.daysOfWeek[6] : this.daysOfWeek[currentDay - 1]
    this.dataSet = Object.keys(dataSetObj).map(e => {
      return {
        date: e === '0' ? this.daysOfWeek[6] : this.daysOfWeek[e - 1],
        timeSpent: dataSetObj[e]
      };
    });
    this.dataSet = [...this.dataSet.slice(1, 7), this.dataSet[0]];
  }
}

export default BarChartWeekly;
