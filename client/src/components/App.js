import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchTasks } from '../actions';
import { pomodoroFinished } from '../actions';
import { pomodoroStarted } from '../actions';
import { proceedInterrupted } from '../actions';
import { showNotification, timeToBeDisplayed } from './helpers';
import Header from './Header';
import Landing from './Landing';
import Dashboard from './Dashboard';
import Stats from './Stats';

class App extends Component {
  async componentDidMount() {
    await this.props.fetchTasks();
    this.processInterrupted();
  }

  constructor() {
    super();
    this.pomoLength = 60 * 25;
    this.state = {
      secondsLeft: this.pomoLength,
      taskId: 0,
      taskName: '',
      tags: new Set(),
      state: 'NEVER_STARTED',
      startedAt: null,
      startAutomatically: false
    };
  }

  processInterrupted() {
    let interrupted = localStorage.interrupted;
    const startAutomatically = localStorage.startAutomatically;
    const timeStamp = Date.now();
    if (interrupted) {
      interrupted = JSON.parse(interrupted);
      const pomosLeft = Math.floor((timeStamp - interrupted.startedAt) / (this.pomoLength * 1000));
      const currentPomoSecondsLeft =
        this.pomoLength - Math.round((timeStamp - interrupted.startedAt - this.pomoLength * pomosLeft * 1000) / 1000);
      let interruptedPomos = [];
      const tags = new Set(interrupted.tags);
      let startedAt = interrupted.startedAt;
      let finishedAt = startedAt + this.pomoLength * 1000;
      for (let i = 0; i < pomosLeft; i++) {
        startedAt = interrupted.startedAt + i * this.pomoLength * 1000;
        finishedAt = startedAt + this.pomoLength * 1000;
        interruptedPomos.push({
          taskId: interrupted.taskId,
          taskName: interrupted.taskName,
          tags: tags,
          startedAt: startedAt,
          finishedAt: finishedAt
        });
      }
      if (interruptedPomos.length) {
        this.props.proceedInterrupted(interruptedPomos);
        startedAt = finishedAt;
      } else {
        this.props.pomodoroStarted(interrupted.taskId);
      }
      this.setState(
        {
          secondsLeft: currentPomoSecondsLeft,
          taskId: interrupted.taskId,
          taskName: interrupted.taskName,
          tags: tags,
          state: 'RUNNING',
          startedAt: startedAt,
          startAutomatically: startAutomatically
        },
        () => {
          this.continueTimer();
        }
      );
    }
  }

  setStartAutomatically(status) {
    this.setState({ startAutomatically: status });
  }

  startTimer(taskId, taskName, tags) {
    window.scrollTo(0, 0);
    if (this.state.state === 'NEVER_STARTED' || this.state.state === 'FINISHED') {
      this.props.pomodoroStarted(taskId);
      if (localStorage.interrupted === undefined) {
        localStorage.interrupted = JSON.stringify({
          taskId: taskId,
          tags: [...tags],
          taskName: taskName,
          startedAt: Date.now()
        });
      }
      this.setState(
        {
          secondsLeft: this.pomoLength,
          state: 'RUNNING',
          taskId: taskId,
          tags: tags,
          taskName: taskName,
          startedAt: Date.now()
        },
        () => {
          this.updateTimer(Date.now());
        }
      );
    }
  }

  pauseTimer() {
    this.setState({ state: 'PAUSED' });
  }

  continueTimer() {
    this.setState({ state: 'RUNNING' }, () => {
      this.updateTimer(Date.now());
    });
  }

  cancelTimer() {
    localStorage.removeItem('interrupted');
    this.setState({ state: 'NEVER_STARTED', secondsLeft: this.pomoLength });
  }

  stopTimer() {
    this.setState({ state: 'FINISHED' });
    this.props.pomodoroFinished(this.state.taskId, this.state.taskName, this.state.tags, this.state.startedAt);
  }

  updateTimer(lastTimeStamp) {
    if (this.state.secondsLeft > 0 && this.state.state === 'RUNNING') {
      setTimeout(() => {
        let timeStamp = Date.now();
        this.setState({
          secondsLeft: this.state.secondsLeft - Math.round((timeStamp - lastTimeStamp) / 1000)
        });
        document.title = timeToBeDisplayed(this.state.secondsLeft);
        this.updateTimer(timeStamp);
      }, 1000);
    } else if (this.state.secondsLeft <= 0) {
      document.title = '00:00';
      this.setState({ state: 'FINISHED', secondsLeft: 0 }, async () => {
        await this.props.pomodoroFinished(
          this.state.taskId,
          this.state.taskName,
          this.state.tags,
          this.state.startedAt
        );
        if (this.state.startAutomatically) {
          this.startTimer(this.state.taskId, this.state.taskName, this.state.tags);
        }
        showNotification(this.state.taskName);
      });
    } else if (this.state.state === 'NEVER_STARTED') {
      this.setState({ secondsLeft: this.pomoLength, taskId: 0, taskName: '' }, () => {
        document.title = 'Pomotasks';
      });
    }
  }

  render() {
    return (
      <BrowserRouter>
        <div>
          <Header />
          <Route exact path="/" component={Landing} />
          <Route
            exact
            path="/dashboard"
            render={props => (
              <Dashboard
                pomoState={{ state: this.state.state, secondsLeft: this.state.secondsLeft }}
                pauseTimer={this.pauseTimer.bind(this)}
                continueTimer={this.continueTimer.bind(this)}
                cancelTimer={this.cancelTimer.bind(this)}
                stopTimer={this.stopTimer.bind(this)}
                startTimer={this.startTimer.bind(this)}
                setStartAutomatically={this.setStartAutomatically.bind(this)}
                pomodoroStarted={pomodoroStarted.bind(this)}
                pomodoroFinished={pomodoroFinished.bind(this)}
                taskId={this.state.taskId}
                {...props}
              />
            )}
          />
          <Route exact path="/stats" component={Stats} />
        </div>
      </BrowserRouter>
    );
  }
}

export default connect(
  null,
  { fetchTasks, pomodoroStarted, pomodoroFinished, proceedInterrupted }
)(App);
