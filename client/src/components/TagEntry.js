import React, { Component } from "react";
import { Checkbox } from "react-bootstrap";

class TagEntry extends Component {
  constructor() {
    super();
    this.state = {
      checked: false
    };
  }

  componentWillReceiveProps(props) {
    this.setState({ checked: props.tagsAssigned.has(props.name) });
  }

  render() {
    return (
      <div className="tag-entry">
        <Checkbox
          checked={this.state.checked}
          onChange={() => {
            this.setState({ checked: !this.state.checked }, () => {
              this.state.checked
                ? this.props.assignTag(this.props.name)
                : this.props.dismissTag(this.props.name);
            });
          }}
        >
          {this.props.name}
        </Checkbox>
      </div>
    );
  }
}

export default TagEntry;
