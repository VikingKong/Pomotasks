import DonutChartFactory from './DonutChartFactory';
class DonutChartTasks extends DonutChartFactory {
  constructor() {
    super();
    this.title = 'Tasks Distribution';
  }

  wrangleData() {
    let dataSetObj = {};

    if (!this.props.filteredPomos.length) {
      this.dataSet = [{ name: 'Nothing here', timeSpent: 1, emptyData: true }];
      return;
    }

    this.props.filteredPomos.forEach(p => {
      const currentPomoTask = p.title;
      const currentPomoTimeSpent = p.finishedAt - p.startedAt;
      if (!dataSetObj.hasOwnProperty(currentPomoTask)) dataSetObj[currentPomoTask] = currentPomoTimeSpent;
      else dataSetObj[currentPomoTask] += currentPomoTimeSpent;
    });
    this.dataSet = Object.keys(dataSetObj).map(e => {
      return { name: e, timeSpent: dataSetObj[e], emptyData: false };
    });
  }
}

export default DonutChartTasks;
