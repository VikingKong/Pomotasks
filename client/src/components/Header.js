import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';

class Header extends Component {
  authButton() {
    return (
      <NavItem className="auth-button" href="/auth/google">
        Sign Up With Google
      </NavItem>
    );
  }

  authDropdown() {
    return (
      <div className="auth-button">
        {this.props.auth.fullname}
        <img className="avatar" src={this.props.auth.photo} alt="" hspace="5" />
      </div>
    );
  }

  logoClass() {
    switch (this.props.auth) {
      case null:
        return '';
      case false:
        return '';
      default:
        return 'navbar';
    }
  }

  renderAuthButton() {
    switch (this.props.auth) {
      case null:
        return this.authButton();
      case false:
        return this.authButton();
      default:
        return (
          <NavDropdown title={this.authDropdown()} id="logout-dropdown">
            <MenuItem href="api/logout">Log out</MenuItem>
          </NavDropdown>
        );
    }
  }

  render() {
    return (
      <Navbar fixedTop={true}>
        <Navbar.Brand className={this.logoClass()}>
          <Link to={this.props.auth ? '/dashboard' : '/'}>Pomotasks</Link>
        </Navbar.Brand>
        <Navbar.Brand>
          <Link to={this.props.auth ? '/stats' : '/'}>Statistics</Link>
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse>
          <Nav pullRight>{this.renderAuthButton()}</Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default connect(mapStateToProps)(Header);
