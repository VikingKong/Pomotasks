import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import leftButton from '../assets/left-arrow.svg';
import rightButton from '../assets/right-arrow.svg';

class VizTimeFrameSwitcher extends Component {
  render() {
    return (
      <div className="viz-time-frame-switcher">
        <Button className="btn viz-time-frame-button" onClick={this.props.backwardAction}>
          <img src={leftButton} alt="" />
        </Button>
        <div className="viz-time-frame-text">
          <h6>{this.props.timeFrameLabel}</h6>
        </div>
        <Button className="btn viz-time-frame-button" onClick={this.props.forwardAction}>
          <img src={rightButton} color="white" alt="" />
        </Button>
      </div>
    );
  }
}

export default VizTimeFrameSwitcher;
