import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import * as d3 from 'd3';
import * as d3Tip from 'd3-tip';
import { overallTime } from './helpers';

class BarChartFactory extends Component {
  constructor() {
    super();
    this.dataSet = [];
    this.currentTick='';
  }

  wrangleData() {}

  initXAxis() {
    return [];
  }

  prepareViz(el) {
    const margin = { top: 50, right: 180, bottom: 50, left: 50 };
    this.width = 950 - margin.left - margin.right;
    this.height = 950 - margin.top - margin.bottom;
    const getRatio = side => (margin[side] / this.width) * 100 + '%';

    const marginRatio = {
      left: getRatio('left'),
      top: getRatio('top'),
      right: getRatio('right'),
      bottom: getRatio('bottom')
    };

    this.tip = d3Tip
      .default()
      .attr('class', 'd3-tip')
      .offset([50, 70])
      .html(
        d =>
          `<span style="color:white"><strong>${overallTime(
            parseInt(d.timeSpent * 3600, 10)
          )}</strong></span>`
      );

    this.g = d3
      .select(el)
      .append('svg')
      .style(
        'padding',
        marginRatio.top +
          ' ' +
          marginRatio.right +
          ' ' +
          marginRatio.bottom +
          ' ' +
          marginRatio.left +
          ' '
      )
      .attr('preserveAspectRatio', 'xMidYMid meet')
      .attr(
        'viewBox',
        '0 0 ' +
          (this.width + margin.left + margin.right) +
          ' ' +
          (this.height + margin.top + margin.bottom) +
          ' '
      )
      .style('width', 950)
      .style('height', 650)
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    this.g.call(this.tip);

    const x = d3
      .scaleBand()
      .domain(this.initXAxis())
      .range([0, this.width])
      .paddingInner(0.3)
      .paddingOuter(0.3);

    const y = d3
      .scaleLinear()
      .domain([0, 1])
      .range([this.height, 0]);

    const leftAxis = d3.axisLeft(y).tickFormat(val => val);
    const bottomAxis = d3.axisBottom(x).tickFormat(val => val);

    this.g
      .append('g')
      .attr('class', 'left-axis')
      .call(leftAxis)
      .selectAll('text')
      .attr('font-size', '16px');

    this.g
      .append('g')
      .attr('class', 'bottom-axis')
      .attr('transform', 'translate(0,' + this.height + ')')
      .call(bottomAxis)
      .selectAll('text')
      .attr('font-size', '16px');

    this.updateViz();
  }

  updateViz() {
    this.wrangleData();
    this.g.selectAll('rect').remove();
    this.g.selectAll('text').remove();
    this.g.selectAll('.tick').remove();

    const x = d3
      .scaleBand()
      .domain(this.dataSet.map(d => d.date))
      .range([0, this.width])
      .paddingInner(0.3)
      .paddingOuter(0.3);
    const y = d3
      .scaleLinear()
      .domain([0, d3.max(this.dataSet, d => d.timeSpent)])
      .range([this.height, 0]);
    const h = d3
      .scaleLinear()
      .domain([0, d3.max(this.dataSet, d => d.timeSpent)])
      .range([0, this.height]);
    const color = d3
      .scaleOrdinal()
      .domain(this.dataSet.map(d => d.date))
      .range(d3.schemeCategory10);
    const leftAxis = d3.axisLeft(y).tickFormat(val => val);
    const bottomAxis = d3.axisBottom(x).tickFormat(val => val);

    this.g
      .append('g')
      .attr('class', 'bottom-axis')
      .attr('transform', 'translate(0,' + this.height + ')')
      .call(bottomAxis)
      .selectAll('text')
      .attr('font-size', '16px')
      .style('fill', d => d === this.currentTick ? 'white' : '');

    this.g
      .append('g')
      .attr('class', 'left-axis')
      .call(leftAxis)
      .selectAll('text')
      .attr('font-size', '16px');

    if (this.dataSet.every(e => e.timeSpent === 0)) return;

    var rectangles = this.g.selectAll('.bar').data(this.dataSet);

    rectangles
      .enter()
      .append('rect')
      .attr('y', d => y(d.timeSpent))
      .attr('x', d => x(d.date))
      .attr('width', x.bandwidth())
      .attr('height', d => h(d.timeSpent))
      .attr('fill', d => color(d.date))
      .on('mouseover', this.tip.show)
      .on('mouseout', this.tip.hide);
  }

  componentDidMount() {
    this.el = ReactDOM.findDOMNode(this);
    this.prepareViz(this.el);
  }

  componentDidUpdate() {
    this.updateViz();
  }

  render() {
    return <div className="barchart" />;
  }
}

export default BarChartFactory;
