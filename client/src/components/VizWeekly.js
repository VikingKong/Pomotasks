import React, { Component } from 'react';
import { connect } from 'react-redux';
import { weekForward, weekBackward } from '../actions';
import BarChartWeekly from './BarChartWeekly';
import DonutChartTasks from './DonutChartTasks';
import DonutChartTags from './DonutChartTags';
import VizTimeFrameSwitcher from './VizTimeFrameSwitcher';
import { setLeadingZero } from './helpers';

class VizWeekly extends Component {
  constructor() {
    super();
    const today = new Date(Date.now());
    const actualDay = today.getDay();
    const day = actualDay === 0 ? 7 : actualDay;
    const date = today.getDate();
    const month = today.getMonth();
    const year = today.getFullYear();
    const monday = new Date(year, month, date - (day - 1));
    const sunday = new Date(year, month, date + (7 - day));
    this.state = {
      timeFrameLabel: `${setLeadingZero(monday.getDate())}.${setLeadingZero(
        monday.getMonth() + 1
      )}.${monday.getFullYear()} - ${setLeadingZero(sunday.getDate())}.${setLeadingZero(
        sunday.getMonth() + 1
      )}.${sunday.getFullYear()}`,
      filteredPomos: []
    };
  }
  componentDidMount() {
    let currentFrame = this.cutTimeFrame(this.props.finishedPomos);
    this.setState(currentFrame);
  }

  componentWillReceiveProps(newProps) {
    let currentFrame = this.cutTimeFrame(newProps.finishedPomos);
    this.setState(currentFrame);
  }

  cutTimeFrame(data) {
    const today = new Date(Date.now());
    const actualDay = today.getDay();
    const day = actualDay === 0 ? 7 : actualDay;
    const date = today.getDate() + this.props.weeklyVizFilter;
    const month = today.getMonth();
    const year = today.getFullYear();
    const monday = new Date(year, month, date - (day - 1));
    const sunday = new Date(year, month, date + (7 - day));
    const leftBoundary = new Date(monday.getFullYear(), monday.getMonth(), monday.getDate(), 0, 0, 0, 0).getTime();
    const rightBoundary = new Date(
      sunday.getFullYear(),
      sunday.getMonth(),
      sunday.getDate(),
      23,
      59,
      59,
      999
    ).getTime();
    let filteredPomos = data.filter(p => p.finishedAt >= leftBoundary && p.finishedAt <= rightBoundary);
    const timeFrameLabel = `${setLeadingZero(monday.getDate())}.${setLeadingZero(
      monday.getMonth() + 1
    )}.${monday.getFullYear()} - ${setLeadingZero(sunday.getDate())}.${setLeadingZero(
      sunday.getMonth() + 1
    )}.${sunday.getFullYear()}`;
    return { filteredPomos, timeFrameLabel };
  }

  async forwardAction() {
    await this.props.weekForward();
    let currentFrame = this.cutTimeFrame(this.props.finishedPomos);
    this.setState(currentFrame);
  }

  async backwardAction() {
    await this.props.weekBackward();
    let currentFrame = this.cutTimeFrame(this.props.finishedPomos);
    this.setState(currentFrame);
  }

  render() {
    return (
      <div className="viz">
        <div className="row">
          <div className="col-md-12">
            <VizTimeFrameSwitcher
              timeFrameLabel={this.state.timeFrameLabel}
              forwardAction={this.forwardAction.bind(this)}
              backwardAction={this.backwardAction.bind(this)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-4">
            <BarChartWeekly filteredPomos={this.state.filteredPomos} />
          </div>
          <div className="col-md-4">
            <DonutChartTasks filteredPomos={this.state.filteredPomos} />
          </div>
          <div className="col-md-4">
            <DonutChartTags filteredPomos={this.state.filteredPomos} />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { weeklyVizFilter: state.weeklyVizFilter, finishedPomos: state.finishedPomos };
}

export default connect(
  mapStateToProps,
  { weekForward, weekBackward }
)(VizWeekly);
