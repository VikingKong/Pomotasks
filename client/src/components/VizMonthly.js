import React, { Component } from 'react';
import { connect } from 'react-redux';
import { monthForward, monthBackward } from '../actions';
import BarChartMonthly from './BarChartMonthly';
import DonutChartTasks from './DonutChartTasks';
import DonutChartTags from './DonutChartTags';
import VizTimeFrameSwitcher from './VizTimeFrameSwitcher';
import { vizCurrentMonth, getNumberOfDaysInMonth } from './helpers';

class VizMonthly extends Component {
  constructor() {
    super();
    const today = new Date(Date.now());
    const year = today.getFullYear();
    const month = today.getMonth();
    this.state = {
      timeFrameLabel: vizCurrentMonth(month, year),
      numOfDays: new Date(today.getFullYear(), today.getMonth() + 1, 0).getDate(),
      filteredPomos: []
    };
  }
  componentDidMount() {
    let currentFrame = this.cutTimeFrame(this.props.finishedPomos);
    this.setState(currentFrame);
  }

  componentWillReceiveProps(newProps) {
    let currentFrame = this.cutTimeFrame(newProps.finishedPomos);
    this.setState(currentFrame);
  }

  cutTimeFrame(data) {
    const today = new Date(Date.now());
    const year = today.getFullYear();
    const month = today.getMonth() + this.props.monthlyVizFilter;
    const numOfDays = getNumberOfDaysInMonth(this.props.monthlyVizFilter);
    const leftBoundary = new Date(year, month, 1, 0, 0, 0, 0).getTime();
    const rightBoundary = new Date(year, month, numOfDays, 23, 59, 59, 999).getTime();
    let filteredPomos = data.filter(
      p => p.finishedAt >= leftBoundary && p.finishedAt <= rightBoundary
    );
    const timeFrameLabel = vizCurrentMonth(month, year);
    return { numOfDays, filteredPomos, timeFrameLabel };
  }

  async forwardAction() {
    await this.props.monthForward();
    let currentFrame = this.cutTimeFrame(this.props.finishedPomos);
    this.setState(currentFrame);
  }

  async backwardAction() {
    await this.props.monthBackward();
    let currentFrame = this.cutTimeFrame(this.props.finishedPomos);
    this.setState(currentFrame);
  }

  render() {
    return (
      <div className="viz">
        <div className="row">
          <div className="col-md-12">
            <VizTimeFrameSwitcher
              timeFrameLabel={this.state.timeFrameLabel}
              forwardAction={this.forwardAction.bind(this)}
              backwardAction={this.backwardAction.bind(this)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-4">
            <BarChartMonthly
              filteredPomos={this.state.filteredPomos}
              numOfDays={this.state.numOfDays}
            />
          </div>
          <div className="col-md-4">
            <DonutChartTasks filteredPomos={this.state.filteredPomos} />
          </div>
          <div className="col-md-4">
            <DonutChartTags filteredPomos={this.state.filteredPomos} />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { monthlyVizFilter: state.monthlyVizFilter, finishedPomos: state.finishedPomos };
}

export default connect(
  mapStateToProps,
  { monthForward, monthBackward }
)(VizMonthly);
