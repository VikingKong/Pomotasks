import BarChartFactory from './BarChartFactory';
import { months } from './helpers.js';

class BarChartYearly extends BarChartFactory {
  initXAxis() {
    return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11].map(m => months[m].slice(0, 3));
  }

  wrangleData() {
    let dataSetObj = {};
    for (let i = 0; i <= 11; i++) {
      dataSetObj[months[i]] = 0;
    }

    this.props.filteredPomos.forEach(p => {
      const currentPomoMonth = new Date(p.finishedAt).getMonth();
      const currentPomoTimeSpent = p.finishedAt - p.startedAt;
      this.currentTick = months[new Date(Date.now()).getMonth()].slice(0, 3);
      dataSetObj[months[currentPomoMonth]] += currentPomoTimeSpent / 1000 / 60 / 60;
    });
    this.dataSet = Object.keys(dataSetObj).map(e => {
      return { date: e.slice(0, 3), timeSpent: dataSetObj[e] };
    });
  }
}

export default BarChartYearly;
