import { connect } from "react-redux";
import TaskInputFactory from "./TaskInputFactory";
import { renameTask } from "../actions";

class UpdateTask extends TaskInputFactory {
  componentDidMount() {
    this.setState({
      showTagEditor: true,
      taskId: this.props.taskId,
      taskName: this.props.taskName,
      tagsAssigned: this.props.tagsAssigned,
      checked: this.props.finished
    });
  }

  ESCHandler() {}

  onSubmit() {
    this.props.renameTask(
      this.state.taskId,
      this.state.taskName,
      this.state.tagsAssigned
    );
    this.setState({ taskName: "", tagsAssigned: new Set() });
    this.props.discardEditor();
  }
}

export default connect(
  null,
  { renameTask }
)(UpdateTask);
