import React, { Component } from "react";
import {
  Form,
  FormGroup,
  FormControl,
  InputGroup,
  Button
} from "react-bootstrap";
import TagEditor from "./TagEditor";

class TaskInputFactory extends Component {
  constructor() {
    super();
    this.state = {
      taskName: "",
      tagsAssigned: new Set(),
      showTagEditor: false,
      eh: this.ESCHandler.bind(this)
    };
  }

  ESCHandler(event) {
    if (event.keyCode === 27) {
      this.hideTagEditor();
      this.setState({ taskName: "", tagsAssigned: new Set() });
    }
  }

  displayTagEditor() {
    this.setState({ showTagEditor: true });
  }

  hideTagEditor() {
    document.removeEventListener("keydown", this.state.eh);
    this.setState({ showTagEditor: false });
  }

  assignTag(tag) {
    this.setState(prevState => ({
      tagsAssigned: prevState.tagsAssigned.add(tag)
    }));
  }

  dismissTag(tag) {
    this.setState(prevState => {
      prevState.tagsAssigned.delete(tag);
      return {
        tagsAssigned: prevState.tagsAssigned
      };
    });
  }

  onSubmit() {}

  render() {
    return (
      <div>
        <Form onSubmit={event => event.preventDefault()}>
          <FormGroup>
            <InputGroup>
              <FormControl
                type="text"
                placeholder="New Task"
                value={this.state.taskName}
                onChange={event =>
                  this.setState({ taskName: event.target.value })
                }
                onKeyUp={event => {
                  if (event.keyCode === 13) {
                    this.onSubmit();
                  }
                }}
                onFocus={() => {
                  document.addEventListener("keydown", this.state.eh);
                  this.displayTagEditor();
                }}
              />
              <InputGroup.Button>
                <Button onClick={this.onSubmit.bind(this)}>Submit</Button>
              </InputGroup.Button>
            </InputGroup>
          </FormGroup>
        </Form>
        <div>
          <TagEditor
            target={this}
            show={this.state.showTagEditor}
            tagsAssigned={this.state.tagsAssigned}
            assignTag={this.assignTag.bind(this)}
            dismissTag={this.dismissTag.bind(this)}
            isLast={this.props.isLast}
          />
        </div>
      </div>
    );
  }
}

export default TaskInputFactory;
