import { combineReducers } from 'redux';
import {
  FETCH_USER,
  FETCH_TASKS,
  POMODORO_STARTED,
  POMODORO_FINISHED,
  PROCEED_INTERRUPTED,
  FILTER_FINISHED_POMODOROS,
  NEW_TASK,
  RENAME_TASK,
  UPDATE_TASK_STATUS,
  TAG_ADDED,
  MONTH_FORWARD,
  MONTH_BACKWARD,
  YEAR_FORWARD,
  YEAR_BACKWARD,
  DAY_FORWARD,
  DAY_BACKWARD,
  WEEK_FORWARD,
  WEEK_BACKWARD
} from '../actions';
import { lastPomo } from '../components/helpers';

function auth(state = null, action) {
  switch (action.type) {
    case FETCH_USER:
      return action.profile || false;
    default:
      return state;
  }
}

function tasks(state = [], action) {
  let newState = [];
  let index = null;
  let taskToChange = null;
  let updatedTaskPomos = [];
  switch (action.type) {
    case FETCH_TASKS:
      return action.tasks.sort((a, b) => lastPomo(b) - lastPomo(a));
    case POMODORO_STARTED:
      index = state.findIndex(el => el._id === action.taskId);
      return state
        .slice(0, 0)
        .concat(state[index])
        .concat(state.filter(el => el._id !== action.taskId));
    case POMODORO_FINISHED:
      index = state.findIndex(el => el._id === action.taskId);
      taskToChange = Object.assign({}, state[index]);
      updatedTaskPomos = taskToChange.pomodoros.slice(0);
      updatedTaskPomos.push({
        startedAt: action.startedAt,
        finishedAt: action.finishedAt
      });
      newState = state
        .slice(0, index)
        .concat(
          [
            {
              _id: action.taskId,
              title: taskToChange.title,
              tags: taskToChange.tags,
              finished: taskToChange.finished,
              pomodoros: updatedTaskPomos
            }
          ],
          state.slice(index + 1)
        )
        .sort((a, b) => lastPomo(b) - lastPomo(a));
      return newState;
    case PROCEED_INTERRUPTED:
      index = state.findIndex(el => el._id === action.pomos[0].taskId);
      taskToChange = Object.assign({}, state[index]);
      updatedTaskPomos = taskToChange.pomodoros.slice(0);
      action.pomos.forEach(item => {
        updatedTaskPomos.push({
          startedAt: item.startedAt,
          finishedAt: item.finishedAt
        });
      });
      newState = state
        .slice(0, index)
        .concat(
          [
            {
              _id: action.pomos[0].taskId,
              title: taskToChange.title,
              tags: taskToChange.tags,
              finished: taskToChange.finished,
              pomodoros: updatedTaskPomos
            }
          ],
          state.slice(index + 1)
        )
        .sort((a, b) => lastPomo(b) - lastPomo(a));
      return newState;
    case NEW_TASK:
      newState = [
        ...state,
        {
          _id: action.taskId,
          title: action.taskName,
          pomodoros: [],
          tags: action.tags,
          finished: false
        }
      ];
      return newState;
    case RENAME_TASK:
      index = state.findIndex(el => el._id === action.taskId);
      taskToChange = Object.assign({}, state[index]);
      taskToChange.title = action.newName;
      taskToChange.tags = action.tags;
      newState = state.slice(0, index).concat([taskToChange], state.slice(index + 1));
      return newState;
    case UPDATE_TASK_STATUS:
      index = state.findIndex(el => el._id === action.taskId);
      taskToChange = Object.assign({}, state[index]);
      taskToChange.finished = action.taskStatus;
      newState = state.slice(0, index).concat([taskToChange], state.slice(index + 1));
      return newState;
    default:
      return state;
  }
}

function finishedPomos(state = [], action) {
  let pomos = [];
  switch (action.type) {
    case FETCH_TASKS:
      action.tasks.forEach(task =>
        task.pomodoros.forEach(pomodoro =>
          pomos.push({
            taskId: task._id,
            title: task.title,
            tags: task.tags,
            startedAt: pomodoro.startedAt,
            finishedAt: pomodoro.finishedAt
          })
        )
      );
      return pomos.sort((a, b) => b.finishedAt - a.finishedAt);
    case POMODORO_FINISHED:
      return [
        {
          taskId: action.taskId,
          title: action.taskName,
          tags: action.tags,
          startedAt: action.startedAt,
          finishedAt: action.finishedAt
        },
        ...state
      ];
    case PROCEED_INTERRUPTED:
      return [
        ...action.pomos
          .map(item => {
            return {
              taskId: item.taskId,
              title: item.taskName,
              tags: item.tags,
              startedAt: item.startedAt,
              finishedAt: item.finishedAt
            };
          })
          .sort((a, b) => b.finishedAt - a.finishedAt),
        ...state
      ];
    case NEW_TASK:
      return state;
    case RENAME_TASK:
      return state.map(pomo =>
        pomo.taskId === action.taskId
          ? {
              title: action.newName,
              tags: action.tags,
              taskId: pomo.taskId,
              startedAt: pomo.startedAt,
              finishedAt: pomo.finishedAt
            }
          : pomo
      );
    case UPDATE_TASK_STATUS:
      return state;
    default:
      return state;
  }
}

function tags(state = new Set(), action) {
  switch (action.type) {
    case FETCH_TASKS:
      return action.tasks.reduce((acc, task) => {
        task.tags.forEach(tag => acc.add(tag));
        return acc;
      }, new Set());
    case TAG_ADDED:
      return new Set(state).add(action.tag);
    default:
      return state;
  }
}

function lastNDays(state = 86400 * 1000 * 7, action) {
  switch (action.type) {
    case FILTER_FINISHED_POMODOROS:
      return action.interval;
    default:
      return state;
  }
}

function monthlyVizFilter(state = 0, action) {
  switch (action.type) {
    case MONTH_FORWARD:
      return state + 1;
    case MONTH_BACKWARD:
      return state - 1;
    default:
      return state;
  }
}

function yearlyVizFilter(state = 0, action) {
  switch (action.type) {
    case YEAR_FORWARD:
      return state + 1;
    case YEAR_BACKWARD:
      return state - 1;
    default:
      return state;
  }
}

function weeklyVizFilter(state = 0, action) {
  switch (action.type) {
    case WEEK_FORWARD:
      return state + 7;
    case WEEK_BACKWARD:
      return state - 7;
    default:
      return state;
  }
}

function dailyVizFilter(state = 0, action) {
  switch (action.type) {
    case DAY_FORWARD:
      return state + 1;
    case DAY_BACKWARD:
      return state - 1;
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  tasks,
  finishedPomos,
  tags,
  auth,
  lastNDays,
  monthlyVizFilter,
  yearlyVizFilter,
  weeklyVizFilter,
  dailyVizFilter
});

export default rootReducer;
