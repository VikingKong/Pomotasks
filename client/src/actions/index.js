import axios from 'axios';
export const FETCH_USER = 'FETCH_USER';
export const FETCH_TASKS = 'FETCH_TASKS';
export const POMODORO_FINISHED = 'POMODORO_FINISHED';
export const POMODORO_STARTED = 'POMODORO_STARTED';
export const NEW_TASK = 'NEW_TASK';
export const RENAME_TASK = 'RENAME_TASK';
export const UPDATE_TASK_STATUS = 'UPDATE_TASK_STATUS';
export const FILTER_FINISHED_POMODOROS = 'FILTER_FINISHED_POMODOROS';
export const TAG_ADDED = 'TAG_ADDED';
export const MONTH_FORWARD = 'MONTH_FORWARD';
export const MONTH_BACKWARD = 'MONTH_BACKWARD';
export const YEAR_FORWARD = 'YEAR_FORWARD';
export const YEAR_BACKWARD = 'YEAR_BACKWARD';
export const WEEK_FORWARD = 'WEEK_FORWARD';
export const WEEK_BACKWARD = 'WEEK_BACKWARD';
export const DAY_FORWARD = 'DAY_FORWARD';
export const DAY_BACKWARD = 'DAY_BACKWARD';
export const PROCEED_INTERRUPTED = 'PROCEED_INTERRUPTED';

export const fetchUser = (history, url) => async dispatch => {
  const result = await axios.get('/api/current-user');
  if (!result.data) {
    history.push('/');
  } else {
    history.push(url);
  }
  dispatch({
    type: FETCH_USER,
    profile: result.data
      ? {
          id: result.data._id,
          fullname: `${result.data.firstname.trim()} ${result.data.lastname.trim()}`,
          photo: result.data.photo
        }
      : null
  });
};

export const fetchTasks = () => async dispatch => {
  try {
    const result = await axios.get('/api/list');
    dispatch({
      type: FETCH_TASKS,
      tasks: result.data.map(task => {
        task.tags = new Set(task.tags);
        return task;
      })
    });
  } catch (e) {
    dispatch({
      type: FETCH_TASKS,
      tasks: []
    });
  }
};

export const filterFinishedPomodoros = interval => {
  return {
    type: FILTER_FINISHED_POMODOROS,
    interval: interval
  };
};

export const pomodoroStarted = taskId => {
  return {
    type: POMODORO_STARTED,
    taskId: taskId
  };
};

export const pomodoroFinished = (taskId, taskName, tags, startedAt) => async dispatch => {
  const finishedAt = Date.now();
  await axios.patch('api/add-pomo', {
    id: taskId,
    startedAt: startedAt,
    finishedAt: finishedAt
  });
  localStorage.removeItem('interrupted');
  dispatch({
    type: POMODORO_FINISHED,
    taskId: taskId,
    taskName: taskName,
    tags: tags,
    startedAt: startedAt,
    finishedAt: finishedAt
  });
};

export const proceedInterrupted = interruptedPomos => async dispatch => {
  await axios.patch('api/add-interrupted', {
    id: interruptedPomos[0].taskId,
    interruptedPomos: interruptedPomos.map(item => {
      return {
        startedAt: item.startedAt,
        finishedAt: item.finishedAt
      };
    })
  });
  dispatch({
    type: PROCEED_INTERRUPTED,
    pomos: interruptedPomos
  });
};

export const createNewTask = (taskName, tagsAssigned) => async dispatch => {
  const result = await axios.post('/api/new-task', {
    title: taskName,
    tags: Array.from(tagsAssigned)
  });
  dispatch({
    type: NEW_TASK,
    taskName: taskName,
    tags: tagsAssigned,
    taskId: result.data
  });
};

export const updateTaskStatus = (taskId, taskStatus) => async dispatch => {
  await axios.patch('/api/toggle-task', { id: taskId });
  dispatch({
    type: UPDATE_TASK_STATUS,
    taskId: taskId,
    taskStatus: taskStatus
  });
};

export const renameTask = (taskId, newName, tagsAssigned) => async dispatch => {
  await axios.patch('/api/rename-task', {
    id: taskId,
    title: newName,
    tags: Array.from(tagsAssigned)
  });
  dispatch({
    type: RENAME_TASK,
    taskId: taskId,
    newName: newName,
    tags: tagsAssigned
  });
};

export const addTag = tag => {
  return {
    type: TAG_ADDED,
    tag: tag
  };
};

export const monthForward = () => {
  return {
    type: MONTH_FORWARD
  };
};
export const monthBackward = () => {
  return {
    type: MONTH_BACKWARD
  };
};

export const yearForward = () => {
  return {
    type: YEAR_FORWARD
  };
};
export const yearBackward = () => {
  return {
    type: YEAR_BACKWARD
  };
};

export const weekForward = () => {
  return {
    type: WEEK_FORWARD
  };
};
export const weekBackward = () => {
  return {
    type: WEEK_BACKWARD
  };
};

export const dayForward = () => {
  return {
    type: DAY_FORWARD
  };
};
export const dayBackward = () => {
  return {
    type: DAY_BACKWARD
  };
};
