const mongoose = require('mongoose');

const Tasks = mongoose.model('tasks');

const listTasks = userId => {
  return Tasks.find({ _user: userId }).select({ __v: false });
};

const addTask = (title, tags, userId) => {
  const newTask = new Tasks({
    title: title,
    _user: userId,
    finished: false,
    pomodoros: [],
    tags: tags
  }).save();
  return newTask;
};

const renameTask = (id, title, tags) => {
  return Tasks.updateOne({ _id: id }, { title: title, tags: tags });
};

const toggleTask = id => {
  Tasks.findOne({ _id: id }, (err, taskToToggle) => {
    if (err) return err;
    taskToToggle.set({ finished: !taskToToggle.finished });
    taskToToggle.save();
  });
};

const addPomo = (id, startedAt, finishedAt) => {
  return Tasks.updateOne({ _id: id }, { $push: { pomodoros: { startedAt: startedAt, finishedAt: finishedAt } } });
};

const addInterrupted = (id, pomos) => {
  return Tasks.updateOne({ _id: id }, { $push: { pomodoros: { $each: pomos } } });
};

module.exports = { listTasks, addTask, renameTask, toggleTask, addPomo, addInterrupted };
